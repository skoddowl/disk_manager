source 'MESSAGES'

printf "$welcome_msg"

printf "diskm# "
while read q; do

  # View all existing disk partitions
  if [ "$q" == "info" ]; then
    fdisk -l
  fi

  # View partitions of a specific hard disk
  if [[ "$q" =~ "disk-info" ]]; then
    fdisk -l ${q[1]}
  fi

  # Delete a hard disk partition
  if [[ "$q" =~ "delete" ]]; then
    fdisk ${q[1]}
  fi

  # Create a hard disk partition
  if [[ "$q" =~ "create" ]]; then
    fdisk ${q[1]}
  fi

  # Format partition (ext4)
  if [[ "$q" =~ "format_ext4" ]]; then
    mkfs.ext4 ${q[1]}
  fi

  # Show help message
  if [ "$q" == "help" ]; then
    printf "$help_message"
  fi

  # Exit
  if [ "$q" == "exit" ]; then
    exit
  fi

  printf "diskm# "
done
